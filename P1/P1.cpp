// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "P1.h"
#include <commdlg.h>
#include <CommCtrl.h>


#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szDrawWndClass[MAX_LOADSTRING] = TEXT("Draw window class";)
WCHAR szDrawWndTitle[MAX_LOADSTRING] = TEXT("Draw window title");
HWND hwndToolBarWnd;
int nCurrentSelectedMenuItem;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    MDICloseProc(HWND,LPARAM);
LRESULT CALLBACK    DrawWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void OnNewDrawWnd(HWND);
void doCreate_ToolBar(HWND hWnd);
void doToolBar_NotifyHandle(LPARAM	lParam);
void doToolBar_AddSTDButton();
void doToolBar_AddUserButton();
void doToolBar_Style(int tbStyle);
void doView_ToolBar(HWND hWnd);
void UpdateMenuState(HWND, int);

BOOL WINAPI InitApplication();


HWND hwndMDIClient, hwndFrame;
int count = 1;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
	InitApplication();

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (!TranslateAccelerator(hwndFrame, hAccelTable, &msg) && !TranslateMDISysAccel(hwndMDIClient, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if (RegisterClassExW(&wcex))
		return 0;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.lpszClassName = L"Draw window class";
	wcex.lpfnWndProc = DrawWndProc;
	wcex.hInstance = hInst;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDC_P1));
	wcex.lpszMenuName = NULL;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if (RegisterClassExW(&wcex))
		return 0;

	return 1;
}

BOOL WINAPI InitApplication()
{
	WNDCLASS wc;
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_P1));
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszClassName = szWindowClass;
	wc.lpszMenuName = MAKEINTRESOURCE(IDC_P1);

	if (!RegisterClass(&wc))
		return FALSE;

	wc.lpfnWndProc = DrawWndProc;
	wc.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_P1));
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.lpszClassName = TEXT("Child class");
	wc.lpszMenuName = NULL;

	if (!RegisterClass(&wc))
		return FALSE;

	return TRUE;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"MyPaint", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//



//void OnNewDrawWnd(HWND hWnd)
//{
//	MDICREATESTRUCT mdiCreate;
//	WCHAR buff[100];
//	wsprintf(buff, L"Noname-%d.drw", count);
//	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));
//
//	mdiCreate.szClass = L"Child class";
//	mdiCreate.szTitle = buff;
//	mdiCreate.cx = mdiCreate.x = CW_USEDEFAULT;
//	mdiCreate.cy = mdiCreate.y = CW_USEDEFAULT;
//	mdiCreate.hOwner = hInst;
//	mdiCreate.style = 0;
//	mdiCreate.lParam = NULL;
//	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
//}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
	case WM_CREATE:
	{
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 3);
		ccs.idFirstChild = 50002;
		hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_HSCROLL | WS_VSCROLL, 0, 0, 0, 0, hWnd, (HMENU)NULL, hInst, (LPVOID)&ccs);
		ShowWindow(hwndMDIClient, SW_SHOW);
		doCreate_ToolBar(hWnd);
		doToolBar_AddUserButton();
		return 0;
	}
	break;

    case WM_COMMAND:
        {
		HMENU hMenu = GetSubMenu(GetMenu(hWnd), 2);
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_FILE_NEW:
			{
				MDICREATESTRUCT mdiCreate;
				WCHAR buff[100];
				wsprintf(buff, L"Noname-%d.drw", count);
				ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));
				mdiCreate.szClass = L"Child class";
				mdiCreate.szTitle = buff;
				mdiCreate.cx = mdiCreate.x = CW_USEDEFAULT;
				mdiCreate.cy = mdiCreate.y = CW_USEDEFAULT;
				mdiCreate.hOwner = hInst;
				mdiCreate.style = 0;
				mdiCreate.lParam = NULL;
				SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
				count++;
			}
			break;

			case ID_DRAW_FONT:
			{
				CHOOSEFONT cf;
				LOGFONT lf;
				ZeroMemory(&cf, sizeof(CHOOSEFONT));
				cf.lStructSize = sizeof(CHOOSEFONT);
				cf.hwndOwner = hWnd;
				cf.lpLogFont = &lf;
				cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
				if (ChooseFont(&cf))
					MessageBox(hWnd, L"Choosefont dialogbox creation success", L"Notification", NULL);
				UpdateMenuState(hWnd, wmId);
			}
			break;

			case ID_DRAW_COLOR:
			{
				CHOOSECOLOR cc;
				COLORREF accCustClr[16];
				DWORD rgbCurrent = RGB(255, 0, 0);
				ZeroMemory(&cc, sizeof(CHOOSECOLOR));
				cc.lStructSize = sizeof(CHOOSECOLOR);
				cc.hwndOwner = hWnd;
				cc.lpCustColors = (LPDWORD)accCustClr;
				cc.rgbResult = rgbCurrent;
				cc.Flags = CC_FULLOPEN | CC_RGBINIT;

				if (ChooseColor(&cc))
					MessageBox(hWnd, L"Choosecolor dialogbox creation success", L"Notification", NULL);
				UpdateMenuState(hWnd, wmId);
			}
			break;
			
			case ID_WINDOW_VIEWHIDE:
				doView_ToolBar(hWnd);
				break;
			case ID_DRAW_LINE:
				UpdateMenuState(hWnd, wmId);
			break;
			case ID_DRAW_RECTANGLE:
				UpdateMenuState(hWnd, wmId);
			break;
			case ID_DRAW_ELLIPSE:
				UpdateMenuState(hWnd, wmId);
				break;
			case ID_DRAW_TEXT:
				UpdateMenuState(hWnd, wmId);
				break;
			case ID_DRAW_SELECTOBJECT:
				UpdateMenuState(hWnd, wmId);
				break;
			case ID_FILE_OPEN:
				MessageBox(hWnd, L"Bạn đã chọn Open", L"Notice", NULL);
				break;
			case ID_FILE_SAVE:
				MessageBox(hWnd, L"Bạn đã chọn Save", L"Notice", NULL);
			break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
				break;
			case ID_WINDOW_TILE:
				SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_HORIZONTAL, 0);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hwndMDIClient, WM_MDICASCADE, MDITILE_ZORDER, 0);
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
				return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
            }
        }
        break;
	case WM_NOTIFY:
		doToolBar_NotifyHandle(lParam);
		break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;

	case WM_SIZE:
	{
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 32, w, h, TRUE);
	}
	break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK  MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
	return 1;
}

LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_MDIACTIVATE:
		break;
	case WM_DESTROY:
		count--;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void doCreate_ToolBar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
	{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	// create a toolbar
	hwndToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	CheckMenuItem(GetMenu(hWnd), ID_WINDOW_VIEWHIDE, MF_CHECKED | MF_BYCOMMAND);
}

void doToolBar_NotifyHandle(LPARAM	lParam)
{
	LPTOOLTIPTEXT   lpToolTipText;
	TCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource
													// lParam: address of TOOLTIPTEXT struct
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}

void doToolBar_AddSTDButton()
{
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{STD_CUT,ID_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_COPY,	ID_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_PASTE, ID_EDIT_PASTE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_DELETE,ID_EDIT_DELETE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	SendMessage(hwndToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}


void doToolBar_AddUserButton()
{

	TBBUTTON tbButtons[] =
	{
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{ STD_PROPERTIES, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_PROPERTIES, ID_DRAW_ELLIPSE,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_PROPERTIES, ID_DRAW_RECTANGLE,TBSTATE_ENABLED,TBSTYLE_BUTTON,0, },
	{ STD_PROPERTIES, ID_DRAW_TEXT,TBSTATE_ENABLED,TBSTYLE_BUTTON, 0, 0 },
	{ STD_PROPERTIES, ID_DRAW_SELECTOBJECT,TBSTATE_ENABLED,TBSTYLE_BUTTON, 0, 0}
	};


	TBADDBITMAP	tbBitmap = { hInst,IDB_HIST_HOT};
	int idx = SendMessage(hwndToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;

    SendMessage(hwndToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
	(LPARAM)(LPTBBUTTON)&tbButtons);
}

void doToolBar_Style(int tbStyle)
{
	ShowWindow(hwndToolBarWnd, SW_HIDE);
	SendMessage(hwndToolBarWnd, TB_SETSTYLE, (WPARAM)0, (LPARAM)(DWORD)tbStyle | CCS_TOP);
	ShowWindow(hwndToolBarWnd, SW_SHOW);
}

void doView_ToolBar(HWND hWnd)
{
	int vFlag = GetMenuState(GetMenu(hWnd), ID_WINDOW_VIEWHIDE, MF_BYCOMMAND) & MF_CHECKED;

	if (vFlag)
	{
		ShowWindow(hwndToolBarWnd, SW_HIDE);
		vFlag = MF_UNCHECKED;
	}
	else
	{
		ShowWindow(hwndToolBarWnd, SW_SHOW);
		vFlag = MF_CHECKED;
	}

	CheckMenuItem(GetMenu(hWnd), ID_WINDOW_VIEWHIDE, vFlag | MF_BYCOMMAND);
}

void UpdateMenuState(HWND hWnd, int newMenuItem)
{
	HMENU hPopup = GetSubMenu(GetMenu(hWnd), 2);
	//Uncheck current selected item
	CheckMenuItem(hPopup, nCurrentSelectedMenuItem, MF_UNCHECKED | MF_BYCOMMAND);
	//check new item
	CheckMenuItem(hPopup, newMenuItem, MF_CHECKED | MF_BYCOMMAND);
	nCurrentSelectedMenuItem = newMenuItem;
}